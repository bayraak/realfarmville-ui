import { Injectable } from '@angular/core';
import { of as observableOf, Observable } from 'rxjs';
import { AnimalCategoriesData, AnimalCategories, Animal } from '../data/animal-categories';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NbAuthService, NbTokenStorage } from '@nebular/auth';

@Injectable()
export class AnimalCategoriesService extends AnimalCategoriesData {

    constructor(private http: HttpClient, private tokenStorage: NbTokenStorage) {
        super();
    }

    getAnimalCategories(): Observable<AnimalCategories[]> {
        const tokenStorage = this.tokenStorage.get() as any;
        const token = `Bearer ${tokenStorage.token}`;

        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': token
        })

        return  this.http.get<AnimalCategories[]>('https://realfarmville-node.herokuapp.com/api/category', { headers: headers });
    }

    getAnimalsByCategory(animalCategory: string): Observable<Animal[]> {
        const tokenStorage = this.tokenStorage.get() as any;
        const token = `Bearer ${tokenStorage.token}`;

        const headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': token
        })

        return  this.http.get<Animal[]>(`https://realfarmville-node.herokuapp.com/api/animal/category/${animalCategory}`,
        { headers: headers });
    }
}
