import { Injectable } from '@angular/core';
import { of as observableOf, Observable } from 'rxjs';
import { ProgressInfo, StatsProgressBarData } from '../data/stats-progress-bar';

@Injectable()
export class StatsProgressBarService extends StatsProgressBarData {
  private progressInfoData: ProgressInfo[] = [
    {
      title: 'Cow Feed (Provender)',
      value: 240,
      activeProgress: 70,
      description: '',
    },
    {
      title: 'Sheep Feed (Provender)',
      value: 123,
      activeProgress: 30,
      description: '',
    },
    {
      title: 'Chicken Feed (Provender)',
      value: 13,
      activeProgress: 55,
      description: ''
    },
  ];

  getProgressInfoData(): Observable<ProgressInfo[]> {
    return observableOf(this.progressInfoData);
  }
}
