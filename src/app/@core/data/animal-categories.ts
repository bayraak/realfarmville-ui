import { Observable } from 'rxjs';

export interface AnimalCategories {
  id: number;
  name: string;
  logoUrl: string;
}

export interface Animal {
  animal_id: string;
  animal_content: string;
  animal_name: string;
  animal_price: number;
  animal_predictedLifetime: number;
  animal_averageProduction: number;
  animal_averageWeight: number;
  animal_origin: string;
  animal_averageConsumption: number;
  animal_purchaseCount: number;
  animal_commentCount: number;
  category_id: number;
  category_name: string;
  category_logoUrl: string;
  animal_createdAt: Date;
  animal_updatedAt: Date;
  badge: boolean;
}

export abstract class AnimalCategoriesData {
  abstract getAnimalCategories(): Observable<AnimalCategories[]>;
  abstract getAnimalsByCategory(animalCategoy: string): Observable<Animal[]>
}
