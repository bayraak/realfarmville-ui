import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'MANAGEMENT PLATFORM',
    group: true,
  },
  {
    title: 'Barn IoT',
    icon: 'toggle-right-outline',
    link: '/pages/iot-dashboard-management',
  },
  {
    title: 'Security Cameras',
    icon: 'video-outline',
    link: '/pages/security-cameras-management',
  },
  {
    title: 'INSIGHTS PLATFORM',
    group: true,
  },
  {
    title: 'E-commerce',
    icon: 'shopping-cart-outline',
    link: '/pages/e-commerce-insights',
    home: true,
  },
  {
    title: 'Farm & Barn',
    icon: 'home-outline',
    link: '/pages/farm-barn-insights',
  },
  {
    title: 'Production & Consumption',
    icon: 'trending-up-outline',
    link: '/pages/production-consumption-insights',
  },
  {
    title: 'BAZAAR PLATFORM',
    group: true,
  },
  {
    title: 'Animal Bazaar',
    icon: 'globe-2-outline',
    link: '/pages/animal-bazaar',
    home: true,
  },
  {
    title: 'Products Bazaar',
    icon: 'sun-outline',
    link: '/pages/products-bazaar',
  },
  {
    title: 'ACCOUNT PLATFORM',
    group: true,
  },
  {
    title: 'Auth',
    icon: 'lock-outline',
    children: [
      {
        title: 'Login',
        link: '/auth/login',
      },
      {
        title: 'Register',
        link: '/auth/register',
      },
      {
        title: 'Request Password',
        link: '/auth/request-password',
      },
      {
        title: 'Reset Password',
        link: '/auth/reset-password',
      },
    ],
  },
];
