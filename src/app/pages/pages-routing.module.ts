import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './management-platform/dashboard/dashboard.component';
import { ProductionComponent } from './management-platform/production/production.component';
import { SecurityCamerasPageComponent } from './management-platform/security-cameras/security-cameras.component';
import { ECommerceComponent } from './insights-platform/e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { FarmBarnInsightsComponent } from './insights-platform/farm-barn/farm-barn-insights.component';
import { AnimalBazaarPageComponent } from './bazaar-platform/animal-bazaar/animal-bazaar.component';
import { ChargeMoneyComponent } from './miscellaneous/charge-money/charge-money.component';
import { ProductionConsumptionComponent } from './insights-platform/production-consumption/production-consumption.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'iot-dashboard-management',
      component: DashboardComponent,
    },
    {
      path: 'production-management',
      component: ProductionComponent,
    },
    {
      path: 'security-cameras-management',
      component: SecurityCamerasPageComponent,
    },
    {
      path: 'e-commerce-insights',
      component: ECommerceComponent,
    },
    {
      path: 'farm-barn-insights',
      component: FarmBarnInsightsComponent,
    },
    {
      path: 'production-consumption-insights',
      component: ProductionConsumptionComponent,
    },
    {
      path: 'animal-bazaar',
      component: AnimalBazaarPageComponent,
    },
    {
      path: '',
      redirectTo: 'iot-dashboard-management',
      pathMatch: 'full',
    },
    {
      path: 'money-charge',
      component: ChargeMoneyComponent,
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
