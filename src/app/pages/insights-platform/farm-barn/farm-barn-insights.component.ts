import { Component, OnDestroy } from '@angular/core';
import { SolarData } from '../../../@core/data/solar';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'ngx-farm-barn-insights',
  templateUrl: './farm-barn-insights.component.html',
})
export class FarmBarnInsightsComponent implements OnDestroy {
  solarValue: number;
  private alive = true;

  constructor(private solarService: SolarData) {
    this.solarService.getSolarData()
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.solarValue = data;
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
