import { Component, OnDestroy } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { NbThemeService } from '@nebular/theme';
import { OutlineData, ProductionConsumptionData } from '../../../../@core/data/production-consumption-analytics';
import { forkJoin } from 'rxjs';


@Component({
  selector: 'ngx-production-consumption-analytics',
  styleUrls: ['./production-consumption-analytics.component.scss'],
  templateUrl: './production-consumption-analytics.component.html',
})
export class ProductionConsumptionAnalyticsComponent implements OnDestroy {
  private alive = true;

  pieChartValue: number;
  chartLegend: {iconColor: string; title: string}[];
  visitorsAnalyticsData: { innerLine: number[]; outerLine: OutlineData[]; };

  constructor(private themeService: NbThemeService,
              private productionConsumptionService: ProductionConsumptionData) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.setLegendItems(theme.variables.visitorsLegend);
      });

    forkJoin(
      this.productionConsumptionService.getInnerLineChartData(),
      this.productionConsumptionService.getOutlineLineChartData(),
      this.productionConsumptionService.getPieChartData(),
    )
      .pipe(takeWhile(() => this.alive))
      .subscribe(([innerLine, outerLine, pieChartValue]: [number[], OutlineData[], number]) => {
        this.visitorsAnalyticsData = {
          innerLine: innerLine,
          outerLine: outerLine,
        };

        this.pieChartValue = pieChartValue;
      });
  }

  setLegendItems(visitorsLegend): void {
    this.chartLegend = [
      {
        iconColor: visitorsLegend.firstIcon,
        title: 'Production',
      },
      {
        iconColor: visitorsLegend.secondIcon,
        title: 'Consumption',
      },
    ];
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
