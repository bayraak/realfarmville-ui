import { NgModule } from '@angular/core';
import {
  NbButtonModule,
  NbCardModule,
  NbProgressBarModule,
  NbTabsetModule,
  NbUserModule,
  NbIconModule,
  NbSelectModule,
  NbListModule,
} from '@nebular/theme';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { ThemeModule } from '../../../@theme/theme.module';
import { ProductionConsumptionComponent } from './production-consumption.component';
import { ChartModule } from 'angular2-chartjs';
import {
  ProductionConsumptionAnalyticsComponent
} from './production-consumption-analytics/production-consumption-analytics.component';
import {
  ProductionConsumptionAnalyticsChartComponent
} from './production-consumption-analytics/production-consumption-analytics-chart/production-consumption-analytics-chart.component';
import { ECommerceProgressSectionComponent } from './progress-section/progress-section.component';
import { ECommerceLegendChartComponent } from './legend-chart/legend-chart.component';

@NgModule({
  imports: [
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbIconModule,
    NbTabsetModule,
    NbSelectModule,
    NbListModule,
    ChartModule,
    NbProgressBarModule,
    NgxEchartsModule,
    NgxChartsModule,
  ],
  declarations: [
    ProductionConsumptionComponent,
    ProductionConsumptionAnalyticsComponent,
    ProductionConsumptionAnalyticsChartComponent,
    ECommerceProgressSectionComponent,
    ECommerceLegendChartComponent
  ],
  providers: [
  ],
})
export class ProductionConsumptionModule { }
