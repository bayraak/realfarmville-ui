import { NbMenuService, NbToastrService } from '@nebular/theme';
import { Component, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CreditCardValidator } from 'angular-cc-library';
import { NbDialogService, NbDialogRef } from '@nebular/theme';
import { UserService } from '../../../@core/mock/users.service';

@Component({
  selector: 'ngx-charge-money',
  styleUrls: ['./charge-money.component.scss'],
  templateUrl: './charge-money.component.html',
})
export class ChargeMoneyComponent {

  form: FormGroup;
  submitted: boolean = false;
  amount: number = 0;

  constructor(private formBuilder: FormBuilder,
    private menuService: NbMenuService,
    private toastrService: NbToastrService,
    private userService: UserService) {
    this.form = new FormGroup({
      cardNumber: new FormControl(''),
      name: new FormControl(''),
      surname: new FormControl(''),
      expiryDate: new FormControl(''),
      cvc: new FormControl('')
    });
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      cardNumber: [''],
      name: [''],
      surname: [''],
      expiryDate: [''],
      cvc: ['']
    });
  }

  onSubmit(form) {
    this.submitted = true;
    console.log(form);
  }

  ngOnDestroy() {
  }

  // dialogRef: any;

  // open(dialog: TemplateRef<any>) {
  //   this.dialogRef = this.dialogService.open(dialog, { context: this.amount.toString() || '0' });
  // }

  charge() {
    // TODO: Implement the charge money logic
    this.toggleLoadingAnimation();
    // this.dialogRef.close();
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  loading = false;
  private index: number = 0;


  toggleLoadingAnimation() {
    this.loading = true;
    setTimeout(() => {
      this.userService.chargeCurrentBalance(this.amount);
      this.loading = false;
      this.showToast('success')
    }, 3000);
  }

  showToast(status) {
    this.index += 1;
    this.toastrService.show(
      status || 'Payment successful',
      `Your account is charged for ${this.amount} €`,
      { status });
  }

  onAmountChange(event) {
    if (event && event.target && event.target.value) {
      this.amount = parseInt(event.target.value);
    }
  }
}
