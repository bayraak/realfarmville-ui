import { NgModule } from '@angular/core';
import { NbButtonModule, NbCardModule, NbLayoutModule, NbInputModule, NbTooltipModule, NbSpinnerModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ThemeModule } from '../../@theme/theme.module';
import { MiscellaneousRoutingModule } from './miscellaneous-routing.module';
import { MiscellaneousComponent } from './miscellaneous.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ChargeMoneyComponent } from './charge-money/charge-money.component';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import {CardModule} from 'ngx-card/ngx-card';


@NgModule({
  imports: [
    ThemeModule,
    NbCardModule,
    NbButtonModule,
    MiscellaneousRoutingModule,
    CreditCardDirectivesModule,
    FormsModule,
    ReactiveFormsModule,
    CardModule,
    NbLayoutModule,
    NbInputModule,
    NbTooltipModule,
    NbSpinnerModule
  ],
  declarations: [
    MiscellaneousComponent,
    NotFoundComponent,
    ChargeMoneyComponent
  ],
})
export class MiscellaneousModule { }
