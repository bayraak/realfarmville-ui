import { NgModule } from '@angular/core';
import { NbMenuModule, NbBadgeModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './management-platform/dashboard/dashboard.module';
import { ECommerceModule } from './insights-platform/e-commerce/e-commerce.module';
import { ProductionModule } from './management-platform/production/production.module';
import { SecurityCamerasPageModule } from './management-platform/security-cameras/security-cameras.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { FarmBarnInsightsModule } from './insights-platform/farm-barn/farm-barn-insights.module';
import { AnimalBazaarPageModule } from './bazaar-platform/animal-bazaar/animal-bazaar.module';
import { ProductionConsumptionModule } from './insights-platform/production-consumption/production-consumption.module'

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    ProductionModule,
    SecurityCamerasPageModule,
    FarmBarnInsightsModule,
    ProductionConsumptionModule,
    AnimalBazaarPageModule,
    NbBadgeModule
  ],
  declarations: [
    PagesComponent,
  ],
})
export class PagesModule {
}
