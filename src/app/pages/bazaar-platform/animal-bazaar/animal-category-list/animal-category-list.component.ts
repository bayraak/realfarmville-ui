import { Component, OnDestroy, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { AnimalCategoriesData, AnimalCategories } from '../../../../@core/data/animal-categories';

@Component({
  selector: 'ngx-animal-category-list',
  styleUrls: ['./animal-category-list.component.scss'],
  templateUrl: './animal-category-list.component.html',
})
export class AnimalCategoryListComponent implements OnInit, OnDestroy {

  private destroy$ = new Subject<void>();
  animalCategories: AnimalCategories[];
  isCategorySelected: boolean = false;

  @Output('categorySelection') emitCategorySelection: EventEmitter<string> = new EventEmitter<string>();

  constructor(private animalCategoriesData: AnimalCategoriesData) { }

  ngOnInit() {
    this.animalCategoriesData.getAnimalCategories().subscribe(res => {
      this.animalCategories = res;
    });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  selectCategory(name: string) {
    this.emitCategorySelection.emit(name);
    this.isCategorySelected = true;
  }

}
