import { Component, OnDestroy, OnInit, Input, TemplateRef, ViewChild } from '@angular/core';
import { Animal } from '../../../../@core/data/animal-categories';
import { NbDialogService, NbDialogRef, NbToastrService } from '@nebular/theme';
import { UserService } from '../../../../@core/mock/users.service';
import { filter, map } from 'rxjs/operators';
import { Observable, Subscriber } from 'rxjs';

@Component({
  selector: 'ngx-animal-list',
  styleUrls: ['./animal-list.component.scss'],
  templateUrl: './animal-list.component.html',
})
export class AnimalListComponent implements OnInit, OnDestroy {

  @Input() animals: Animal[];

  currentBalance: number = 0;
  currentBalance$ = new Observable<number>();
  lastPurchasedAnimal: Animal = null;
  private index: number = 0;

  ngOnInit() {
    this.currentBalance$ = this.userService.getCurrentBalance();
    this.currentBalance$.subscribe(val => {
      this.currentBalance = val;
    });
    this.userService.chargeCurrentBalance(3434);
  }

  ngOnDestroy() {
    if (this.dialogRef && this.dialogRef.componentRef) {
      this.dialogRef.componentRef.destroy()
    }
  }

  constructor(private dialogService: NbDialogService, 
    private userService: UserService,
    private toastrService: NbToastrService,
    ) {
  }

  dialogRef: NbDialogRef<any>;

  open(dialog: TemplateRef<any>) {
    const balance = this.currentBalance ? this.currentBalance.toString() : '0'
    this.dialogRef = this.dialogService.open(dialog, { context: balance });
  }

  purchase(animal: Animal) {
    // TODO: Implement the animal purchase logic
    this.lastPurchasedAnimal = animal;
    this.userService.chargeCurrentBalance(-animal.animal_price);
    this.dialogRef.close();
    setTimeout(() => {
      this.showToast('success');
    }, 1000)
  }

  showToast(status) {
    this.index += 1;
    this.toastrService.show(
      status || 'successful',
      `You're purchase was succesful for: ${this.lastPurchasedAnimal.animal_name}`,
      { status });
  }
}