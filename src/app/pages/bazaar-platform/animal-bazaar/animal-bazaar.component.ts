import { Component, OnDestroy, OnInit } from '@angular/core';
import { Animal, AnimalCategoriesData } from '../../../@core/data/animal-categories';

@Component({
  selector: 'ngx-animal-bazaar-page',
  styleUrls: ['./animal-bazaar.component.scss'],
  templateUrl: './animal-bazaar.component.html',
})
export class AnimalBazaarPageComponent implements OnDestroy, OnInit {

  selectedCategory: string = undefined;
  animals: Animal[];

  loading = false;

  constructor(private animalCategoriesData: AnimalCategoriesData) {
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.selectedCategory = undefined;
  }

  loadSelectedAnimalCategory(animalCategory: string) {
    this.loading = true;
    this.animalCategoriesData.getAnimalsByCategory(animalCategory).subscribe(res => {
      let maxProfit = this.getMostProfitableIndex(res);
      if (maxProfit > -1) {
        res[maxProfit].badge = true;
      }
      this.animals = res;
      this.loading = false;
    });
  }

  categorySelected(event) {
    this.selectedCategory = event;
    this.loadSelectedAnimalCategory(event);
  }

  goBack() {
    this.selectedCategory = undefined;
  }

  getMostProfitableIndex(arr: Animal[]): number {
    if (arr.length === 0) {
      return -1;
    }

    let maxValue = arr[0].animal_price / (arr[0].animal_averageProduction / arr[0].animal_averageConsumption);
    let maxElement = arr[0];
    let maxIndex = 0;
    let currentValue = arr[0].animal_price / (arr[0].animal_averageProduction / arr[0].animal_averageConsumption);

    for (let i = 1; i < arr.length; i++) {
      if (arr[i].animal_averageProduction < 1 || arr[i].animal_averageConsumption < 1) {
        maxIndex = -1;
      }
      currentValue = arr[i].animal_price / ( arr[i].animal_averageProduction / arr[i].animal_averageConsumption);
      if (currentValue > maxValue) {
        maxIndex = i;
        maxValue = arr[i].animal_price / ( arr[i].animal_averageProduction / arr[i].animal_averageConsumption);
        maxElement = arr[i];
      }
    }

    return maxIndex;
  }
}
