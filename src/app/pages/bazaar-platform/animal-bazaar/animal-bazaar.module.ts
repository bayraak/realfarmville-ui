import { NgModule } from '@angular/core';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbTabsetModule,
  NbUserModule,
  NbRadioModule,
  NbSelectModule,
  NbListModule,
  NbIconModule,
  NbBadgeModule,
  NbTooltipModule,
  NbDialogModule,
  NbToastrModule
} from '@nebular/theme';
import { NgxEchartsModule } from 'ngx-echarts';

import { ThemeModule } from '../../../@theme/theme.module';
import { AnimalBazaarPageComponent } from './animal-bazaar.component';
import { AnimalCategoryListComponent } from './animal-category-list/animal-category-list.component';
import { FormsModule } from '@angular/forms';
import { AnimalListComponent } from './animal-list/animal-list.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    NgxEchartsModule,
    NbBadgeModule,
    NbDialogModule,
    NbTooltipModule,
    RouterModule
  ],
  declarations: [
    AnimalBazaarPageComponent,
    AnimalCategoryListComponent,
    AnimalListComponent
  ]
})
export class AnimalBazaarPageModule { }
