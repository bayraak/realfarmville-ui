const express = require('express');
const path = require('path');
const version = require('./package.json').version;
const name = require('./package.json').name;

console.log(`${name} - v${version}`);

const app = express();

app.use(express.static(__dirname + '/dist'));

app.get('/*', function (req, res) {

  res.sendFile(path.join(__dirname + '/dist/index.html'));
});

app.listen(process.env.PORT || 8080);
